using System;
using System.Collections.Generic;
using Encompass;
using Kav;
using Encompass3D.Components;
using Encompass3D.Utility;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Linq;

namespace Encompass3D.Renderers
{
    public class SceneRenderer : GeneralRenderer
    {
        private GraphicsDevice graphicsDevice { get; }
        private SpriteBatch spriteBatch { get; }

        private Kav.Renderer renderer { get; }

        private RenderTargetBinding[] gBuffer { get; }

        private RenderTarget2D gPosition { get; }
        private RenderTarget2D gNormal { get; }
        private RenderTarget2D gAlbedo { get; }
        private RenderTarget2D gMetallicRoughness { get; }

        private RenderTarget2D deferredTarget { get; }
        private RenderTargetCube shadowCubeMap { get; }
        private DirectionalShadowMapData directionalShadowMapData { get; }
        private readonly Dictionary<Kav.MeshPart, List<Matrix>> instanceMap = new Dictionary<MeshPart, List<Matrix>>();

        private IEnumerable<(Kav.MeshPart, Matrix)> MeshPartTransforms
        {
            get
            {
                foreach (var entity in ReadEntitiesAsEnumerable<ModelComponent>())
                {
                    var modelComponent = GetComponent<ModelComponent>(entity);

                    var transformComponent = GetComponent<TransformComponent>(entity);
                    foreach (var mesh in modelComponent.model.Meshes)
                    {
                        foreach (var meshPart in mesh.MeshParts)
                        {
                            yield return (meshPart, transformComponent.matrix);
                        }
                    }

                }
            }
        }

        private AmbientLight AmbientLight
        {
            get
            {
                if (SomeComponent<AmbientLightComponent>())
                {
                    return new AmbientLight(ReadComponent<AmbientLightComponent>().color);
                }
                else
                {
                    return new AmbientLight(Color.Black);
                }
            }
        }

        private IEnumerable<PointLight> PointLights
        {
            get
            {
                foreach (var entity in ReadEntitiesAsEnumerable<PointLightComponent>())
                {
                    var transformComponent = GetComponent<TransformComponent>(entity);
                    var pointLightComponent = GetComponent<PointLightComponent>(entity);

                    yield return new PointLight(
                        transformComponent.position,
                        pointLightComponent.color,
                        pointLightComponent.intensity
                    );
                }
            }
        }


        private Kav.DirectionalLight? DirectionalLight()
        {
            if (SomeComponent<DirectionalLightComponent>())
            {
                var entity = ReadEntity<DirectionalLightComponent>();
                var transformComponent = GetComponent<TransformComponent>(entity);
                var directionalLightComponent = GetComponent<DirectionalLightComponent>(entity);

                return new Kav.DirectionalLight(
                    transformComponent.forward,
                    directionalLightComponent.color,
                    directionalLightComponent.intensity
                );
            }
            else
            {
                return null;
            }
        }

        private IEnumerable<(MeshSprite, Matrix)> MeshSpriteTransforms()
        {
            foreach (var entity in ReadEntitiesAsEnumerable<MeshSpriteComponent>())
            {
                var transformComponent = GetComponent<TransformComponent>(entity);
                var spriteComponent = GetComponent<MeshSpriteComponent>(entity);

                yield return (new MeshSprite(graphicsDevice, spriteComponent.sprite, spriteComponent.billboardConstraint), transformComponent.matrix);
            }
        }
        private void CollectInstances()
        {
            foreach (var list in instanceMap.Values)
            {
                list.Clear();
            }

            foreach (var modelEntity in ReadEntities<InstancedModelComponent>())
            {
                if (HasComponent<TransformComponent>(modelEntity))
                {
                    var modelComponent = GetComponent<InstancedModelComponent>(modelEntity);
                    var transformComponent = GetComponent<TransformComponent>(modelEntity);

                    foreach (var mesh in modelComponent.model.Meshes)
                    {
                        foreach (var meshPart in mesh.MeshParts)
                        {
                            if (!instanceMap.ContainsKey(meshPart))
                            {
                                instanceMap.Add(meshPart, new List<Matrix>());
                            }
                            else

                                instanceMap[meshPart].Add(transformComponent.matrix);
                        }
                    }
                }
            }
        }


        public SceneRenderer(GraphicsDevice graphicsDevice)
        {
            this.graphicsDevice = graphicsDevice;
            var renderDimensionsX = graphicsDevice.PresentationParameters.BackBufferWidth;
            var renderDimensionsY = graphicsDevice.PresentationParameters.BackBufferHeight;

            this.renderer = new Kav.Renderer(graphicsDevice);

            deferredTarget = new RenderTarget2D(
                graphicsDevice,
                renderDimensionsX,
                renderDimensionsY,
                false,
                SurfaceFormat.Color,
                DepthFormat.Depth24Stencil8,
                0,
                RenderTargetUsage.PreserveContents
            );

            gPosition = new RenderTarget2D(
                graphicsDevice,
                renderDimensionsX,
                renderDimensionsY,
                false,
                SurfaceFormat.Vector4,
                DepthFormat.Depth24,
                0,
                RenderTargetUsage.PreserveContents
            );

            gNormal = new RenderTarget2D(
                graphicsDevice,
                renderDimensionsX,
                renderDimensionsY,
                false,
                SurfaceFormat.Vector4,
                DepthFormat.None
            );

            gAlbedo = new RenderTarget2D(
                graphicsDevice,
                renderDimensionsX,
                renderDimensionsY,
                false,
                SurfaceFormat.Color,
                DepthFormat.None
            );

            gMetallicRoughness = new RenderTarget2D(
                graphicsDevice,
                renderDimensionsX,
                renderDimensionsY,
                false,
                SurfaceFormat.HalfVector2,
                DepthFormat.None
            );

            gBuffer = new RenderTargetBinding[4] {
                new RenderTargetBinding(gPosition),
                new RenderTargetBinding(gNormal),
                new RenderTargetBinding(gAlbedo),
                new RenderTargetBinding(gMetallicRoughness)
            };

            shadowCubeMap = Kav.Renderer.CreateShadowCubeMap(
                graphicsDevice,
                2048
            );

            directionalShadowMapData = Kav.Renderer.CreateDirectionalShadowMaps(
                graphicsDevice,
                2048,
                4
            );

            spriteBatch = new SpriteBatch(graphicsDevice);
        }

        public override void Render()
        {
            CollectInstances();

            foreach (var cameraEntity in ReadEntities<CameraComponent>().ToArray().OrderBy(cam => GetComponent<CameraComponent>(cam).layer))
            {
                var cameraComponent = GetComponent<CameraComponent>(cameraEntity);
                var transformComponent = GetComponent<TransformComponent>(cameraEntity);

                var cameraRightVector = Vector3.Transform(Vector3.UnitX, transformComponent.orientation);

                var cameraUpVector = Vector3.Transform(Vector3.Transform(Vector3.UnitZ, transformComponent.orientation),
                                                       Quaternion.CreateFromAxisAngle(cameraRightVector, cameraComponent.verticalTilt));

                var cameraForwardVector = Vector3.Transform(Vector3.Transform(Vector3.UnitY, transformComponent.orientation),
                                                            Quaternion.CreateFromAxisAngle(cameraRightVector, cameraComponent.verticalTilt));


                var camera = new Kav.PerspectiveCamera(
                    transformComponent.position + cameraComponent.offset,
                    cameraForwardVector,
                    cameraUpVector,
                    cameraComponent.fieldOfView,
                    16.0f / 9.0f,
                    cameraComponent.nearPlane,
                    cameraComponent.farPlane
                );

                graphicsDevice.SetRenderTargets(gBuffer);
                graphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Black, 1f, 0);

                graphicsDevice.SetRenderTarget(deferredTarget);
                graphicsDevice.Clear(ClearOptions.Target | ClearOptions.DepthBuffer, Color.Black, 1f, 0);
                graphicsDevice.DepthStencilState = DepthStencilState.Default;

                foreach (var meshPart in instanceMap.Keys)
                {
                    renderer.RenderGBufferInstanced(
                        gBuffer,
                        deferredTarget,
                        camera,
                        meshPart,
                        instanceMap[meshPart]
                    );
                }

                renderer.RenderGBufferIndexed(gBuffer, camera, MeshPartTransforms);

                renderer.RenderDepthIndexed(deferredTarget, camera, MeshPartTransforms);

                renderer.RenderAmbientLight(deferredTarget, gPosition, gAlbedo, AmbientLight);

                var closest = PointLights.OrderBy(l => Vector3.DistanceSquared(l.Position, camera.Position));

                foreach (var pointLight in closest.Take(16))
                {
                    foreach (CubeMapFace face in Enum.GetValues(typeof(CubeMapFace)))
                    {
                        graphicsDevice.SetRenderTarget(shadowCubeMap, face);
                        graphicsDevice.Clear(Color.White);
                    }

                    renderer.RenderPointShadowMapIndexed(
                        shadowCubeMap,
                        camera,
                        MeshPartTransforms,
                        pointLight
                    );

                    renderer.RenderPointLight(
                        deferredTarget,
                        gPosition,
                        gAlbedo,
                        gNormal,
                        gMetallicRoughness,
                        shadowCubeMap,
                        camera,
                        pointLight
                    );
                }

                var directionalLight = DirectionalLight();

                if (directionalLight.HasValue)
                {
                    directionalShadowMapData.Clear(graphicsDevice);

                    renderer.PrepareDirectionalShadowData(
                        directionalShadowMapData,
                        camera,
                        directionalLight.Value
                    );

                    renderer.RenderDirectionalShadowsIndexed(
                        directionalShadowMapData,
                        MeshPartTransforms
                    );

                    foreach (var meshPart in instanceMap.Keys)
                    {

                        renderer.RenderDirectionalShadowsInstanced(
                            directionalShadowMapData,
                            meshPart,
                            instanceMap[meshPart]
                        );

                    }

                    renderer.RenderDirectionalLight(
                        deferredTarget,
                        gPosition,
                        gAlbedo,
                        gNormal,
                        gMetallicRoughness,
                        directionalShadowMapData,
                        camera,
                        directionalLight.Value
                    );
                }

                renderer.MeshSpriteRender(
                        deferredTarget,
                        camera,
                        MeshSpriteTransforms(),
                        AmbientLight,
                        closest.Take(8),
                        DirectionalLight()
                    );

                graphicsDevice.SetRenderTarget(null);
                spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, null, null, null);
                spriteBatch.Draw(deferredTarget, new Rectangle(0, 0, graphicsDevice.Viewport.Width, graphicsDevice.Viewport.Height), Color.White);
                spriteBatch.End();

            }
        }

    }
}