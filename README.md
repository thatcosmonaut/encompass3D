﻿# Encompass3D

**Encompass3D** is a set of engines, components, messages, and renderers for making first-person 3D games with [FNA](https://fna-xna.github.io/), [Encompass](http://moonside.games/docs/encompass/), and [BulletSharp](https://andrestraks.github.io/BulletSharp/), a C# wrapper around [Bullet Physics](https://pybullet.org/wordpress/).

Like Encompass itself, **Encompass3D** is licensed under the [zlib license](https://en.wikipedia.org/wiki/Zlib_License). 