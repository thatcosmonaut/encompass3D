using Encompass;
using Kav;

namespace Encompass3D.Messages
{
    public struct ClearForcesMessage : IMessage
    {
        public Entity entity;
    }
}
