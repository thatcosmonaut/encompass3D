﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct SetTransformMessage : IMessage
    {
        public Entity entity;
        public Vector3 position;
        public Quaternion orientation;
    }
}
