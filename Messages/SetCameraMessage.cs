﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct SetCameraMessage : IMessage
    {
        public Entity entity;
        public float fieldOfView;
        public float nearPlane;
        public float farPlane;
        public float verticalTilt;
        public Vector3 offset;

        public int layer;
    }
}
