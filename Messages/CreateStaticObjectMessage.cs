using Encompass;
using BulletSharp;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct CreateStaticObjectMessage : IMessage
    {
        public Entity entity;
        public CollisionShape shape;
        public Vector3 position;
    }
}
