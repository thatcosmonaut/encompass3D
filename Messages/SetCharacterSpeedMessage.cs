using Encompass;
using Kav;

namespace Encompass3D.Messages
{
    public struct SetCharacterSpeedMessage : IMessage
    {
        public Entity entity;
        public float speedMultiplier;
    }
}
