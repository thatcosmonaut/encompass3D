﻿using Encompass;

namespace Encompass3D.Messages
{
    public struct InputMessage : IMessage
    {
        public string inputName;
        public bool held;
    }
}
