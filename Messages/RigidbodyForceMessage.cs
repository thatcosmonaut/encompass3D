﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct RigidbodyForceMessage : IMessage
    {
        public Entity entity;
        public Vector3 vector;
    }
}
