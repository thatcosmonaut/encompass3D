using Encompass;
using Kav;
using Microsoft.Xna.Framework.Graphics;

namespace Encompass3D.Messages
{
    public struct SetMeshSpriteMessage : IMessage
    {
        public Entity entity;
        public Texture2D sprite;
        public SpriteBillboardConstraint billboardConstraint;
    }
}
