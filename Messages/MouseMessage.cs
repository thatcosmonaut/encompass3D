﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct MouseMessage : IMessage
    {
        public Vector2 position;
        public Vector2 delta;
    }
}
