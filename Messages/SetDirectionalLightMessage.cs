using Encompass;
using Kav;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct SetDirectionalLightMessage : IMessage
    {
        public Entity entity;
        public Color color;
        public float intensity;
    }
}
