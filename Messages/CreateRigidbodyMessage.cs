﻿using Encompass;
using BulletSharp;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct CreateRigidbodyMessage : IMessage
    {
        public Entity entity;
        public RigidBodyConstructionInfo constructionInfo;
        public Vector3 position;
        public Quaternion orientation;
        public Vector3 angularFactor;
        public Vector3 linearFactor;
    }
}
