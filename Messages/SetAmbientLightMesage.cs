using Encompass;
using Kav;
using Microsoft.Xna.Framework;

namespace Encompass3D.Messages
{
    public struct SetAmbientLightMessage : IMessage
    {
        public Entity entity;
        public Color color;
    }
}
