using Encompass;
using Kav;

namespace Encompass3D.Messages
{
    public struct SetInstancedModelMessage : IMessage
    {
        public Entity entity;
        public Model model;
    }
}
