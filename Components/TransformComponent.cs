﻿using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Components
{
    public struct TransformComponent : IComponent
    {
        public Vector3 position;
        public Quaternion orientation;

        public Matrix matrix
        {
            get
            {
                return Matrix.CreateFromQuaternion(orientation) * Matrix.CreateTranslation(position);
            }
        }

        public Vector3 forward
        {
            get
            {
                return Vector3.Transform(Vector3.UnitY, orientation);
            }
        }

        public Vector3 right
        {
            get
            {
                return Vector3.Transform(Vector3.UnitX, orientation);
            }
        }

        public Vector3 up
        {
            get
            {
                return Vector3.Transform(Vector3.UnitZ, orientation);
            }
        }


    }
}
