using Encompass;
using Kav;

namespace Encompass3D.Components
{
    public struct InstancedModelComponent : IComponent
    {
        public Model model;
    }
}
