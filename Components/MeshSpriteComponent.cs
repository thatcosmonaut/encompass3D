using Encompass;
using Kav;
using Microsoft.Xna.Framework.Graphics;

namespace Encompass3D.Components
{
    public struct MeshSpriteComponent : IComponent
    {
        public Texture2D sprite;
        public SpriteBillboardConstraint billboardConstraint;
    }
}