using BulletSharp;
using Encompass;

namespace Encompass3D.Components
{
    public struct StaticObjectComponent : IComponent
    {
        public CollisionObject collisionObject;
        public bool inWorld;
    }
}
