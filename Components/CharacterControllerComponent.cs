﻿using Encompass;

namespace Encompass3D.Components
{
    public struct CharacterControllerComponent : IComponent
    {
        public float speed;
        public float speedMultiplier;
        public float lookSensitivity;
        public float jumpForce;
        public int maxJumps;
        public int numJumps;
        public bool crouching;
    }
}
