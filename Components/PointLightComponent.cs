using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Components
{
    public struct PointLightComponent : IComponent
    {
        public Color color;
        public float intensity;

    }
}