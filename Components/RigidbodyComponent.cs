﻿using BulletSharp;
using Encompass;

namespace Encompass3D.Components
{
    public struct RigidbodyComponent : IComponent
    {
        public RigidBody rigidbody;
    }
}
