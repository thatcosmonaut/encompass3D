using Encompass;
using Microsoft.Xna.Framework;

namespace Encompass3D.Components
{
    public struct DirectionalLightComponent : IComponent
    {
        public Color color;
        public float intensity;
    }
}