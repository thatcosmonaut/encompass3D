﻿using Encompass;
using Kav;

namespace Encompass3D.Components
{
    public struct ModelComponent : IComponent
    {
        public Model model;
    }
}
