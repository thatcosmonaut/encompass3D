﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using BulletSharp;
using BulletSharp.Math;

namespace Encompass3D.Utility
{
    public static class ShapeGenerator
    {
        public static CollisionShape CreateShapeFromGlTFModel(Smuggler.ModelData model)
        {
            TriangleMesh triangleMesh = new TriangleMesh();
            foreach(var mesh in model.Meshes)
            {
                foreach(var meshPart in mesh.MeshParts)
                {

                    foreach(var triangle in meshPart.Triangles)
                    {

                        var a = meshPart.Positions[triangle.A];
                        var b = meshPart.Positions[triangle.B];
                        var c = meshPart.Positions[triangle.C];

                        triangleMesh.AddTriangle(a.ToBulletVector(), b.ToBulletVector(), c.ToBulletVector());
                    }
                }
            }

            var shape = new BvhTriangleMeshShape(triangleMesh, true);

            return shape;
        }

    }
}
