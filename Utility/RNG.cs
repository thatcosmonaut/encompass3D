﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Encompass3D.Utility
{
    public static class RNG
    {
        private static float SquareRootOfTwo = (float)Math.Sqrt(2f);

        private static int[] hash;
        private const int hashMask = 255;

        private static float[] gradients1D = {
            1f, -1f
        };

        private const int gradientsMask1D = 1;

        private static Vector2[] gradients2D = {
            new Vector2( 1f, 0f),
            new Vector2(-1f, 0f),
            new Vector2( 0f, 1f),
            new Vector2( 0f,-1f),
            Vector2.Normalize(new Vector2( 1f, 1f)),
            Vector2.Normalize(new Vector2(-1f, 1f)),
            Vector2.Normalize(new Vector2( 1f,-1f)),
            Vector2.Normalize(new Vector2(-1f,-1f))
        };

        private const int gradientsMask2D = 7;



        private static Random random;

        public static void Initialize()
        {
            random = new Random();

            hash = new int[(hashMask + 1) * 2];

            for (int i = 0; i < hash.Length / 2; i++)
            {           
                hash[i + (hash.Length/2 - 1)] = hash[i] = RNG.GetInt(0, hashMask + 1);
            }

        }

        public static float GetFloat(float min = 0f, float max = 1f)
        {
            return (float)random.NextDouble() * (max - min) + min;
        }

        public static int GetInt(int min, int max)
        {
            return (int)Math.Floor(GetFloat(min, max));
        }

        public static int GetIntInclusive(int min, int max)
        {
            return GetInt(min, max + 1);
        }

        public static T GetRandomItem<T>(this T[] array)
        {
            return array[GetInt(0, array.Length)];
        }

        public static XmlNode GetRandomNode(this XmlNodeList nodes)
        {
            return nodes[GetInt(0, nodes.Count)];
        }

        private static float Smooth(float t)
        {
            return t * t * t * (t * (t * 6f - 15f) + 10f);
        }

        public static float PerlinNoise(Vector2 point, float frequency, int octaves = 1, float persistence = 0.5f, float lacunarity = 2f)
        {
            float value = 0f;
            float amplitude = 1f;
            float range = 1f;

            for (int octave = 0; octave < octaves; octave++)
            {
                point *= frequency;
                int ix0 = (int)System.Math.Floor(point.X);
                int iy0 = (int)System.Math.Floor(point.Y);
                float tx0 = point.X - ix0;
                float ty0 = point.Y - iy0;
                float tx1 = tx0 - 1f;
                float ty1 = ty0 - 1f;
                ix0 &= hashMask;
                iy0 &= hashMask;
                int ix1 = ix0 + 1;
                int iy1 = iy0 + 1;


                int h0 = hash[ix0];
                int h1 = hash[ix1];
                Vector2 g00 = gradients2D[hash[h0 + iy0] & gradientsMask2D];
                Vector2 g10 = gradients2D[hash[h1 + iy0] & gradientsMask2D];
                Vector2 g01 = gradients2D[hash[h0 + iy1] & gradientsMask2D];
                Vector2 g11 = gradients2D[hash[h1 + iy1] & gradientsMask2D];

                float v00 = Vector2.Dot(g00, new Vector2(tx0, ty0));
                float v10 = Vector2.Dot(g10, new Vector2(tx1, ty0));
                float v01 = Vector2.Dot(g01, new Vector2(tx0, ty1));
                float v11 = Vector2.Dot(g11, new Vector2(tx1, ty1));

                float tx = Smooth(tx0);
                float ty = Smooth(ty0);

                value += MathHelper.SmoothStep(
                                    MathHelper.SmoothStep(v00, v10, tx),
                                    MathHelper.SmoothStep(v01, v11, tx),
                                    ty) * SquareRootOfTwo * amplitude;

                frequency *= lacunarity;
                amplitude *= persistence;
                range += amplitude;

            }

            return value / range;
        }

    }
}
