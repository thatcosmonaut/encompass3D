﻿using BulletSharp;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encompass3D.Utility
{
    public struct Collision
    {
        public ManifoldPoint manifoldPoint;
        public CollisionObjectWrapper collisionObjectWrapper0;
        public int partId0;
        public int index0;
        public CollisionObjectWrapper collisionObjectWrapper1;
        public int partId1;
        public int index1;
    }

    public class Collisions : ContactResultCallback
    {
        public List<Collision> collisions;

        public override float AddSingleResult(ManifoldPoint cp, CollisionObjectWrapper colObj0Wrap, int partId0, int index0, CollisionObjectWrapper colObj1Wrap, int partId1, int index1)
        {
            if(collisions == null)
            {
                collisions = new List<Collision>();
            }

            Collision collision;
            collision.manifoldPoint = cp;
            collision.collisionObjectWrapper0 = colObj0Wrap;
            collision.partId0 = partId0;
            collision.index0 = index0;
            collision.collisionObjectWrapper1 = colObj1Wrap;
            collision.partId1 = partId1;
            collision.index1 = index1;
            collisions.Add(collision);

            return 0;
        }
    }
}
