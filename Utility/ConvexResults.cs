﻿using BulletSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Encompass3D.Utility
{
    public class ConvexResults : ConvexResultCallback
    {
        public List<LocalConvexResult> localConvexResults;

        public override float AddSingleResult(LocalConvexResult convexResult, bool normalInWorldSpace)
        {
            if(localConvexResults == null)
            {
                localConvexResults = new List<LocalConvexResult>();
            }

            localConvexResults.Add(convexResult);

            return 0;
        }
    }
}
