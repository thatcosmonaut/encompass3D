﻿using BulletSharp;
using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;
using Encompass3D.Utility;
using Microsoft.Xna.Framework;
using System;
using System.Diagnostics;
using System.Linq;

namespace Encompass3D.Engines
{
    [Writes(typeof(CharacterControllerComponent))]
    [Reads(typeof(RigidbodyComponent), typeof(CharacterControllerComponent), typeof(CameraComponent), typeof(PhysicsComponent))]
    [Sends(typeof(RigidbodyForceMessage), typeof(SetRigidbodyMessage), typeof(SetCameraMessage), typeof(RigidbodyImpulseMessage), typeof(CreateRigidbodyMessage))]
    [Receives(typeof(InputMessage), typeof(MouseMessage), typeof(SetCharacterSpeedMessage), typeof(CreateCharacterControllerMessage))]
    public class CharacterControllerEngine : Engine
    {
        public override void Update(double dt)
        {

            foreach (var createCharacter in ReadMessages<CreateCharacterControllerMessage>())
            {
                CharacterControllerComponent characterComponent;
                characterComponent.lookSensitivity = createCharacter.lookSensitivity;
                characterComponent.speed = createCharacter.speed;
                characterComponent.jumpForce = createCharacter.jumpForce;
                characterComponent.maxJumps = createCharacter.maxJumps;
                characterComponent.numJumps = 0;
                characterComponent.speedMultiplier = 1f;
                characterComponent.crouching = false;
                SetComponent(createCharacter.entity, characterComponent);
            }

            if (!SomeMessage<MouseMessage>() || !SomeComponent<PhysicsComponent>())
            {
                return;
            }

            var physics = ReadComponent<PhysicsComponent>();

            foreach (var character in ReadEntities<CharacterControllerComponent>())
            {
                if (!HasComponent<RigidbodyComponent>(character))
                {
                    continue;
                }

                var rigidbody = GetComponent<RigidbodyComponent>(character);
                var camera = GetComponent<CameraComponent>(character);
                var controller = GetComponent<CharacterControllerComponent>(character);
                var shape = (CapsuleShapeZ)rigidbody.rigidbody.CollisionShape;

                foreach (var setSpeed in ReadMessages<SetCharacterSpeedMessage>())
                {
                    if (setSpeed.entity == character)
                    {
                        controller.speedMultiplier = setSpeed.speedMultiplier;
                    }
                }

                var jumpFrom = Matrix.CreateFromQuaternion(rigidbody.rigidbody.Orientation.ToXnaQuaternion()) *
                            Matrix.CreateTranslation(rigidbody.rigidbody.CenterOfMassPosition.X,
                                                     rigidbody.rigidbody.CenterOfMassPosition.Y,
                                                     rigidbody.rigidbody.CenterOfMassPosition.Z);

                var jumpTo = Matrix.CreateFromQuaternion(rigidbody.rigidbody.Orientation.ToXnaQuaternion()) *
                            Matrix.CreateTranslation(rigidbody.rigidbody.CenterOfMassPosition.X,
                                                     rigidbody.rigidbody.CenterOfMassPosition.Y,
                                                     rigidbody.rigidbody.CenterOfMassPosition.Z - shape.Margin / 100f);

                ConvexResults jumpResults = new ConvexResults();


                physics.world.ConvexSweepTest(shape, jumpFrom.ToBulletMatrix(), jumpTo.ToBulletMatrix(), jumpResults);

                if (jumpResults.localConvexResults != null &&
                    jumpResults.localConvexResults.Any(o => o.HitCollisionObject != rigidbody.rigidbody))
                {
                    controller.numJumps = 0;
                }

                Vector3 movementVector = Vector3.Zero;

                bool crouching = false;
                foreach (var input in ReadMessages<InputMessage>())
                {
                    if (input.inputName.ToLower() == "forwards")
                    {
                        movementVector += new Vector3(0f, 1f, 0f);
                    }
                    if (input.inputName.ToLower() == "backwards")
                    {
                        movementVector += new Vector3(0f, -1f, 0f);
                    }
                    if (input.inputName.ToLower() == "right")
                    {
                        movementVector += new Vector3(1f, 0f, 0f);
                    }
                    if (input.inputName.ToLower() == "left")
                    {
                        movementVector += new Vector3(-1f, 0f, 0f);
                    }
                    if (input.inputName.ToLower() == "jump" && !input.held && (controller.numJumps < controller.maxJumps || controller.maxJumps == -1))

                    {
                        RigidbodyImpulseMessage jumpMessage;
                        jumpMessage.vector = Vector3.UnitZ * controller.jumpForce;
                        jumpMessage.entity = character;
                        SendMessage(jumpMessage);
                        controller.numJumps++;
                    }
                    if (input.inputName.ToLower() == "crouch")
                    {
                        crouching = true;
                    }
                }

                SetCameraMessage setCamera;
                setCamera.entity = character;
                setCamera.farPlane = camera.farPlane;
                setCamera.nearPlane = camera.nearPlane;
                setCamera.fieldOfView = camera.fieldOfView;
                setCamera.verticalTilt = camera.verticalTilt;
                setCamera.offset = camera.offset;
                setCamera.layer = 0;

                if (crouching && !controller.crouching)
                {
                    controller.crouching = true;
                    CreateRigidbodyMessage createRigidbody;
                    createRigidbody.entity = character;
                    createRigidbody.angularFactor = Vector3.Zero;
                    createRigidbody.linearFactor = Vector3.One;
                    createRigidbody.orientation = rigidbody.rigidbody.Orientation.ToXnaQuaternion();
                    createRigidbody.position = rigidbody.rigidbody.CenterOfMassPosition.ToXnaVector();
                    createRigidbody.constructionInfo =
                    new RigidBodyConstructionInfo(1f, null, new CapsuleShapeZ(shape.Radius * 0.5f, shape.HalfHeight / 2f));
                    SendMessage(createRigidbody);
                    setCamera.offset = new Vector3(setCamera.offset.X, setCamera.offset.Y, setCamera.offset.Z / 2f);
                    controller.speed /= 2f;
                }
                else if (!crouching && controller.crouching)
                {
                    controller.crouching = false;
                    CreateRigidbodyMessage createRigidbody;
                    createRigidbody.entity = character;
                    createRigidbody.angularFactor = Vector3.Zero;
                    createRigidbody.linearFactor = Vector3.One;
                    createRigidbody.orientation = rigidbody.rigidbody.Orientation.ToXnaQuaternion();
                    createRigidbody.position = rigidbody.rigidbody.CenterOfMassPosition.ToXnaVector();
                    createRigidbody.constructionInfo =
                    new RigidBodyConstructionInfo(1f, null, new CapsuleShapeZ(shape.Radius * 2f, shape.HalfHeight * 8));
                    SendMessage(createRigidbody);
                    setCamera.offset = new Vector3(setCamera.offset.X, setCamera.offset.Y, setCamera.offset.Z * 2f);
                    controller.speed *= 2f;
                }

                if (movementVector != Vector3.Zero)
                {
                    movementVector = Vector3.Transform(Vector3.Normalize(movementVector) * controller.speed * controller.speedMultiplier, rigidbody.rigidbody.Orientation.ToXnaQuaternion());
                }

                SetRigidbodyMessage setRigidbody;
                setRigidbody.entity = character;
                setRigidbody.position = rigidbody.rigidbody.CenterOfMassPosition.ToXnaVector() + movementVector * (float)dt;
                setRigidbody.orientation = rigidbody.rigidbody.Orientation.ToXnaQuaternion();

                var mouseMessage = ReadMessage<MouseMessage>();

                var euler = Vector3.UnitZ * mouseMessage.delta.X * (float)dt * controller.lookSensitivity;
                euler = Vector3.Transform(euler, rigidbody.rigidbody.Orientation.ToXnaQuaternion());
                setRigidbody.orientation = Quaternion.Concatenate(setRigidbody.orientation, Quaternion.CreateFromYawPitchRoll(euler.X, euler.Y, euler.Z));
                setCamera.verticalTilt = camera.verticalTilt += mouseMessage.delta.Y * (float)dt * controller.lookSensitivity;

                if (controller.speedMultiplier != 0)
                {
                    SendMessage(setRigidbody);
                }
                SendMessage(setCamera);
                SetComponent(character, controller);

            }


        }
    }
}
