﻿using Encompass;
using Encompass3D.Messages;
using Encompass3D.Components;
using Encompass3D.Utility;
using BulletSharp;
using Microsoft.Xna.Framework;
using System.Collections.Generic;
using System.Diagnostics;
using BulletSharp.SoftBody;
using System;

namespace Encompass3D.Engines
{
    [Reads(typeof(RigidbodyComponent), typeof(TransformComponent), typeof(PhysicsComponent), typeof(StaticObjectComponent))]
    [Writes(typeof(RigidbodyComponent), typeof(PhysicsComponent), typeof(StaticObjectComponent))]
    [Receives(typeof(CreateRigidbodyMessage), typeof(RigidbodyForceMessage),
                typeof(RigidbodyTorqueMessage), typeof(SetRigidbodyMessage),
                typeof(RigidbodyImpulseMessage), typeof(ClearForcesMessage),
                typeof(CreateStaticObjectMessage))]
    [Sends(typeof(SetTransformMessage))]
    public class PhysicsEngine : Engine
    {
        private Dictionary<Entity, RigidbodyComponent> rigidbodies;

        public override void Update(double dt)
        {

            if (rigidbodies == null)
            {
                rigidbodies = new Dictionary<Entity, RigidbodyComponent>();
            }
            else
            {
                rigidbodies.Clear();
            }

            var physicsEntity = ReadEntity<PhysicsComponent>();
            var physics = ReadComponent<PhysicsComponent>();

            var rigidbodyEntities = ReadEntities<RigidbodyComponent>();
            foreach (var entity in rigidbodyEntities)
            {
                var rigidbody = GetComponent<RigidbodyComponent>(entity);
                var transform = GetComponent<TransformComponent>(entity);
                rigidbodies.Add(entity, rigidbody);
            }

            foreach (var clearForces in ReadMessages<ClearForcesMessage>())
            {
                rigidbodies[clearForces.entity].rigidbody.ClearForces();
            }

            foreach (var setRigidbody in ReadMessages<SetRigidbodyMessage>())
            {
                var rigidbody = rigidbodies[setRigidbody.entity];

                var matrix = Matrix.CreateFromQuaternion(setRigidbody.orientation) *
                            Matrix.CreateTranslation(setRigidbody.position.X, setRigidbody.position.Y, setRigidbody.position.Z);

                rigidbody.rigidbody.WorldTransform = matrix.ToBulletMatrix();
                rigidbody.rigidbody.MotionState.WorldTransform = matrix.ToBulletMatrix();
                rigidbody.rigidbody.ClearForces();
            }

            foreach (var applyForce in ReadMessages<RigidbodyForceMessage>())
            {
                var rigidbody = rigidbodies[applyForce.entity];
                rigidbody.rigidbody.ApplyCentralForce(applyForce.vector.ToBulletVector());

            }

            foreach (var impulse in ReadMessages<RigidbodyImpulseMessage>())
            {
                var rigidbody = rigidbodies[impulse.entity];
                rigidbody.rigidbody.ApplyCentralImpulse(impulse.vector.ToBulletVector());
            }

            physics.world.StepSimulation((float)dt);
            SetComponent(physicsEntity, physics);

            foreach (var kv in rigidbodies)
            {
                var entity = kv.Key;
                var rigidbody = kv.Value;

                SetComponent(entity, rigidbody);

                SetTransformMessage setTransform;
                setTransform.entity = kv.Key;
                setTransform.orientation = kv.Value.rigidbody.Orientation.ToXnaQuaternion();
                setTransform.position = kv.Value.rigidbody.CenterOfMassPosition.ToXnaVector();
                SendMessage(setTransform);
            }

            foreach (var createRigidbody in ReadMessages<CreateRigidbodyMessage>())
            {
                RigidbodyComponent rigidbody;

                var matrix = Matrix.CreateFromQuaternion(createRigidbody.orientation) *
                            Matrix.CreateTranslation(createRigidbody.position.X, createRigidbody.position.Y, createRigidbody.position.Z);

                createRigidbody.constructionInfo.MotionState = new DefaultMotionState(matrix.ToBulletMatrix());
                createRigidbody.constructionInfo.AngularSleepingThreshold = 0f;
                createRigidbody.constructionInfo.LinearSleepingThreshold = 0f;

                var body = new RigidBody(createRigidbody.constructionInfo);
                physics.world.AddRigidBody(body);

                rigidbody.rigidbody = body;
                rigidbody.rigidbody.LinearFactor = createRigidbody.linearFactor.ToBulletVector();
                rigidbody.rigidbody.AngularFactor = createRigidbody.angularFactor.ToBulletVector();

                SetComponent(createRigidbody.entity, rigidbody);

            }

            foreach (var createStatic in ReadMessages<CreateStaticObjectMessage>())
            {
                var staticObject = new CollisionObject
                {
                    CollisionShape = createStatic.shape,
                    WorldTransform = BulletSharp.Math.Matrix.Translation(createStatic.position.ToBulletVector()),
                    CollisionFlags = CollisionFlags.StaticObject,
                    ActivationState = ActivationState.IslandSleeping
                };
                physics.world.AddCollisionObject(staticObject,
                CollisionFilterGroups.StaticFilter,
                CollisionFilterGroups.StaticFilter ^ CollisionFilterGroups.AllFilter);

                StaticObjectComponent staticObjectComponent;
                staticObjectComponent.collisionObject = staticObject;
                staticObjectComponent.inWorld = true;
                SetComponent(createStatic.entity, staticObjectComponent);
            }

        }
    }
}
