using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;

namespace Encompass3D.Engines
{
    [Reads(typeof(PointLightComponent), typeof(AmbientLightComponent), typeof(DirectionalLightComponent))]
    [Writes(typeof(PointLightComponent), typeof(AmbientLightComponent), typeof(DirectionalLightComponent))]
    [Receives(typeof(SetPointLightMessage), typeof(SetAmbientLightMessage), typeof(SetDirectionalLightMessage))]
    public class LightEngine : Engine
    {
        public override void Update(double dt)
        {

            foreach (var setLight in ReadMessages<SetPointLightMessage>())
            {
                PointLightComponent pointLightComponent;
                pointLightComponent.color = setLight.color;
                pointLightComponent.intensity = setLight.intensity;
                SetComponent(setLight.entity, pointLightComponent);
            }

            foreach (var setLight in ReadMessages<SetAmbientLightMessage>())
            {
                AmbientLightComponent ambientLightComponent;
                ambientLightComponent.color = setLight.color;
                SetComponent(setLight.entity, ambientLightComponent);

            }

            foreach (var setLight in ReadMessages<SetDirectionalLightMessage>())
            {
                DirectionalLightComponent directionalLight;
                directionalLight.color = setLight.color;
                directionalLight.intensity = setLight.intensity;
            }

        }
    }
}
