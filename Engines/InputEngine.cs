﻿using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;
using System.Linq;

namespace Encompass3D.Engines
{
    [Reads(typeof(InputComponent))]
    [Sends(typeof(InputMessage), typeof(MouseMessage))]
    public class InputEngine : Engine
    {
        private MouseState lastMouseState;
        private bool lastMouseStateSet = false;
        private List<Keys> heldKeys;
        private List<Buttons> heldButtons;
        private Game game;

        public InputEngine(Game game)
        {
            this.game = game;
            heldKeys = new List<Keys>();
            heldButtons = new List<Buttons>();
            lastMouseState = Mouse.GetState();
            lastMouseStateSet = false;
        }

        public override void Update(double dt)
        {

            if (!game.IsActive)
            {
                lastMouseStateSet = false;
                return;
            }

            if (!SomeComponent<InputComponent>())
            {
                return;
            }

            var input = ReadComponent<InputComponent>();

            var keyboardState = Keyboard.GetState();
            var mouseState = Mouse.GetState();
            var controllerState = GamePad.GetState(0);

            heldKeys = heldKeys.Where(k => !keyboardState.IsKeyUp(k)).ToList();
            heldButtons = heldButtons.Where(b => !controllerState.IsButtonUp(b)).ToList();

            Mouse.SetPosition(game.Window.ClientBounds.Width / 2, game.Window.ClientBounds.Width / 2);

            if (mouseState.LeftButton == ButtonState.Pressed)
            {
                InputMessage inputMessage;
                inputMessage.inputName = input.leftMouseButton;
                inputMessage.held = lastMouseState.LeftButton == ButtonState.Pressed;
                SendMessage(inputMessage);
            }
            else if (mouseState.RightButton == ButtonState.Pressed)
            {
                InputMessage inputMessage;
                inputMessage.inputName = input.rightMouseButton;
                inputMessage.held = lastMouseState.RightButton == ButtonState.Pressed;
                SendMessage(inputMessage);
            }
            else if (mouseState.MiddleButton == ButtonState.Pressed)
            {
                InputMessage inputMessage;
                inputMessage.inputName = input.middleMouseButton;
                inputMessage.held = lastMouseState.MiddleButton == ButtonState.Pressed;
                SendMessage(inputMessage);
            }

            foreach (var name in input.inputs.Keys)
            {
                bool pressed = false;
                bool held = false;

                foreach (var key in input.inputs[name].keys)
                {
                    if (keyboardState.IsKeyDown(key))
                    {
                        pressed = true;
                        if (!heldKeys.Contains(key))
                        {
                            heldKeys.Add(key);
                        }
                        else
                        {
                            held = true;
                        }
                        break;
                    }
                }

                foreach (var button in input.inputs[name].buttons)
                {
                    if (controllerState.IsButtonDown(button))
                    {
                        pressed = true;
                        if (!heldButtons.Contains(button))
                        {
                            heldButtons.Add(button);
                        }
                        else
                        {
                            held = true;
                        }
                        break;
                    }
                }

                if (pressed)
                {
                    InputMessage inputMessage;
                    inputMessage.inputName = name;
                    inputMessage.held = held;
                    SendMessage(inputMessage);
                }
            }

            MouseMessage mouseMessage;
            mouseMessage.position = new Vector2(mouseState.X, mouseState.Y);
            mouseMessage.delta = Vector2.Zero;

            if (lastMouseStateSet)
            {
                mouseMessage.delta = new Vector2(lastMouseState.X, lastMouseState.Y) - new Vector2(mouseState.X, mouseState.Y);
            }

            SendMessage(mouseMessage);

            lastMouseState = Mouse.GetState();
            lastMouseStateSet = true;

        }
    }
}
