﻿using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;
using Encompass3D.Utility;
using System.Collections.Generic;

namespace Encompass3D.Engines
{
    [Reads(typeof(TransformComponent), typeof(RigidbodyComponent))]
    [Writes(typeof(TransformComponent))]
    [Receives(typeof(SetTransformMessage))]
    public class TransformEngine : Engine
    {
        private Dictionary<Entity, TransformComponent> transforms;

        public override void Update(double dt)
        {
            if(transforms == null)
            {
                transforms = new Dictionary<Entity, TransformComponent>();
            }
            else
            {
                transforms.Clear();
            }

            foreach(var setTransform in ReadMessages<SetTransformMessage>())
            {
                TransformComponent transform;

                if (transforms.ContainsKey(setTransform.entity))
                {
                    transform = transforms[setTransform.entity];
                }

                transform.orientation = setTransform.orientation;
                transform.position = setTransform.position;

                transforms[setTransform.entity] = transform;
            }

            foreach(var kv in transforms)
            {
                SetComponent(kv.Key, kv.Value);
            }

        }
    }
}
