﻿using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;
using Microsoft.Xna.Framework;

namespace Encompass3D.Engines
{
    [Reads(typeof(CameraComponent))]
    [Writes(typeof(CameraComponent))]
    [Receives(typeof(SetCameraMessage))]
    public class CameraEngine : Engine
    {
        public override void Update(double dt)
        {
            foreach (var setCamera in ReadMessages<SetCameraMessage>())
            {
                CameraComponent cameraComponent;
                cameraComponent.farPlane = setCamera.farPlane;
                cameraComponent.fieldOfView = setCamera.fieldOfView;
                cameraComponent.nearPlane = setCamera.nearPlane;
                cameraComponent.verticalTilt = MathHelper.Clamp(setCamera.verticalTilt, -MathHelper.PiOver2, MathHelper.PiOver2);
                cameraComponent.offset = setCamera.offset;
                cameraComponent.layer = setCamera.layer;
                SetComponent(setCamera.entity, cameraComponent);
            }
        }
    }
}
