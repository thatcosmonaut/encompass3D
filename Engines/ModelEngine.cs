﻿using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;

namespace Encompass3D.Engines
{
    [Reads(typeof(ModelComponent), typeof(InstancedModelComponent))]
    [Writes(typeof(ModelComponent), typeof(InstancedModelComponent))]
    [Receives(typeof(SetModelMessage), typeof(SetInstancedModelMessage))]
    public class ModelEngine : Engine
    {
        public override void Update(double dt)
        {
            foreach (var setModel in ReadMessages<SetModelMessage>())
            {
                ModelComponent model;
                model.model = setModel.model;

                SetComponent(setModel.entity, model);
            }

            foreach (var setModel in ReadMessages<SetInstancedModelMessage>())
            {
                InstancedModelComponent model;
                model.model = setModel.model;

                SetComponent(setModel.entity, model);
            }
        }
    }
}
