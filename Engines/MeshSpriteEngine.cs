using Encompass;
using Encompass3D.Components;
using Encompass3D.Messages;

namespace Encompass3D.Engines
{
    [Reads(typeof(MeshSpriteComponent))]
    [Writes(typeof(MeshSpriteComponent))]
    [Receives(typeof(SetMeshSpriteMessage))]
    public class MeshSpriteEngine : Engine
    {
        public override void Update(double dt)
        {
            foreach (var setMeshSprite in ReadMessages<SetMeshSpriteMessage>())
            {
                MeshSpriteComponent sprite;
                sprite.sprite = setMeshSprite.sprite;
                sprite.billboardConstraint = setMeshSprite.billboardConstraint;
                SetComponent(setMeshSprite.entity, sprite);
            }

        }
    }
}
